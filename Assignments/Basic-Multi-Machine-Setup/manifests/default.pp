# Node appserver
node /^appserver/ {
  # execute 'apt-update'
  exec { 'apt-update':
    path => ['/usr/bin', '/bin', '/usr/sbin'],
    command => 'apt-get update',
  }

  # install curl package
  package { 'curl':
    ensure => installed,
    require => Exec['apt-update'],
  }

  # execute 'nodejs-version'
  exec { 'nodejs-version':
    path => ['/usr/bin', '/bin', '/usr/sbin'],
    command => 'curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -';
  }

  # install nodejs package
  package { 'nodejs':
    ensure  => present,
    require => [
      Exec['apt-update'],
      Exec['nodejs-version']
    ]; 
  }
}

# Node dbserver
node /^dbserver/ {
  # Setup mysql service
  service { 'mysql':
    require => Package['mysql-server'],
    ensure  => running; 
  }

  # Install mysql server
  package { 'mysql-server':
    ensure  => [present, installed, running];
  } 
}



# Node web
node /^web/ {
    service { 'nginx':
      ensure  => [present, installed, running],
      require => Package['nginx'];
  }
}

# Node tst0, tst1, tst2
node /^tst\d+$/ {
  exec { 'apt-update': 
    path => ['/usr/bin', '/bin', '/usr/sbin'],
    command => 'apt-get update';
  }
}

node default {}
