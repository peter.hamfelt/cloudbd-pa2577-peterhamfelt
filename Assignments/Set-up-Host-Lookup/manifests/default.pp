### Nodes ###

node /^appserver/ {
  # Node for appserver
  include curl
  include nodejs
}

node /^dbserver/ {
  # Node for dbserver
  include mysql
}

node default {}

### Classes ###

class apt_get_update {
  # execute 'apt-update'
  exec { 'apt-update':
    path => ['/usr/bin', '/bin', '/usr/sbin'],
    command => 'apt-get update',
  }
}

class curl {
  # install curl package
  include apt_get_update
  package { 'curl':
    ensure => installed,
    require => Class['apt_get_update'],
  }
}

class nodejs_version {
  # execute 'nodejs-version'
  exec { 'nodejs-version':
    path => ['/usr/bin', '/bin', '/usr/sbin'],
    command => 'curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -';
  }
}

class nodejs {  
  # install nodejs package
  include apt_get_update
  include nodejs_version
  package { 'nodejs':
    ensure  => present,
    require => [
      Class['apt_get_update'],
      Class['nodejs_version']
    ];
  }
}

class mysql {
  # Setup mysql service
  service { 'mysql':
    require => Package['mysql-server'],
    ensure  => running; 
  }

  # Install mysql server
  package { 'mysql-server':
    ensure  => [present, running];
  }
}
